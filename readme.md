# (著す) arawasu

A simple program for rendering handlebar templates from the command line.

# Building
Use Cargo to build/run the app
```
cargo build --release
```
the app is now in the target/release folder, see cargo for more details

# Example
```
cargo run -- -s -p example/partials -i example/test.hbs -d example/data.json
```