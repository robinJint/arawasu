use std::borrow::ToOwned;
use std::convert::Into;

use rlua;
use serde_json;

use crate::Error;

/// Convert a lua value to a json value, using json::Null for types that cannot be represented as json, like closures.
/// uses the '#'/len to decide if a table is an array or a map.
pub(crate) fn from_lua_value(val: rlua::Value) -> Result<serde_json::Value, crate::Error> {
    use rlua::Value;
    use serde_json::{Map, Value as Json};

    fn value_as_string(v: Value) -> Option<String> {
        if let Value::String(s) = v {
            s.to_str().ok().map(ToOwned::to_owned)
        } else {
            None
        }
    }

    let result = match val {
        Value::Nil => Json::Null,
        Value::Boolean(b) => Json::Bool(b),
        Value::Integer(i) => Json::Number(i.into()),
        Value::Number(f) => Json::Number(serde_json::Number::from_f64(f).ok_or(Error::Unknown)?),
        Value::String(s) => Json::String(s.to_str()?.to_owned()),
        Value::Table(t) => {
            if t.len()? > 0 {
                let vec = t
                    .sequence_values()
                    .map(|res: Result<Value, rlua::Error>| {
                        res.map_err(Into::into).and_then(from_lua_value)
                    })
                    .collect::<Result<Vec<Json>, Error>>()?;

                Json::Array(vec)
            } else {
                let mut map = Map::new();

                for item in t.pairs() {
                    let (k, v) = item?;

                    let k_str = if let Some(s) = value_as_string(k) {
                        s
                    } else {
                        continue;
                    };

                    map.insert(k_str, from_lua_value(v)?);
                }

                Json::Object(map)
            }
        }
        _ => Json::Null,
    };

    Ok(result)
}
