use std::ffi::OsStr;
use std::fs::{self, File};
use std::io::{stdout, Read, Write};
use std::path::PathBuf;
use std::str::FromStr;

use derive_more::From;
use handlebars::Handlebars;
use serde_json::json;
use structopt::StructOpt;

mod helpers;

#[derive(Debug)]
enum DataFormat {
    Json,
    Yaml,
    Toml,
    Lua,
}

impl FromStr for DataFormat {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let s: &str = &s.to_owned().to_ascii_lowercase();
        let result = match s {
            "json" => DataFormat::Json,
            "yaml" => DataFormat::Yaml,
            "yml" => DataFormat::Yaml,
            "toml" => DataFormat::Toml,
            "lua" => DataFormat::Lua,
            _ => return Err("Not a data format"),
        };

        Ok(result)
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "arawasu")]
struct Opt {
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,

    /// Quit on render errors
    #[structopt(short = "s", long = "strict")]
    strict: bool,

    /// File with data that can be used by the templates.
    /// The filetype is detected based on the extension, but can be overritten by the --data-type option
    #[structopt(short = "d", long = "data", parse(from_os_str))]
    data: Option<PathBuf>,

    /// The format for the data file. Can be json, yaml or toml
    #[structopt(short = "t", long = "data-type")]
    data_format: Option<DataFormat>,

    /// Directories where to search for partials/templates.
    /// Partials should have the extension .hbs.
    #[structopt(short = "p", long = "partials", parse(from_os_str))]
    partial_dirs: Vec<PathBuf>,

    /// Output folder, all input files are written to this directory/filename.
    /// The hbs extension is removed from the filename, and the directory is created (if required).
    /// If no folder is specified then stdout is used (usefull for testing).
    #[structopt(short = "o", long = "output", parse(from_os_str))]
    output: Option<PathBuf>,

    /// Files to process
    #[structopt(short = "i", long = "input", parse(from_os_str))]
    files: Vec<PathBuf>,
}

#[derive(Debug, From)]
enum Error {
    TemplateFile(handlebars::TemplateFileError),
    TemplateRender(handlebars::TemplateRenderError),
    IO(std::io::Error),
    Unknown,
    Json(serde_json::Error),
    Yaml(serde_yaml::Error),
    Toml(toml::de::Error),
    Lua(rlua::Error),
}

fn main() -> Result<(), Error> {
    // setup
    let opt = Opt::from_args();
    let mut handlebars = Handlebars::new();

    // read the data
    let data = if let Some(path) = &opt.data {
        let mut file = File::open(path)?;
        let data_format = opt
            .data_format
            .or_else(|| {
                path.extension()
                    .and_then(OsStr::to_str)
                    .and_then(|ext| DataFormat::from_str(ext).ok())
            })
            .unwrap_or(DataFormat::Json);

        match data_format {
            DataFormat::Json => serde_json::from_reader(&mut file)?,
            DataFormat::Yaml => serde_yaml::from_reader(&mut file)?,
            DataFormat::Toml => {
                let mut buf = String::new();
                file.read_to_string(&mut buf)?;
                toml::from_str(&buf)?
            }
            DataFormat::Lua => {
                let lua = rlua::Lua::new();
                lua.context(|context| -> Result<serde_json::Value, Error> {
                    let mut buf = Vec::new();
                    file.read_to_end(&mut buf)?;
                    let lua_result: rlua::Value = context.load(&buf).eval()?;
                    helpers::from_lua_value(lua_result)
                })?
            }
        }
    } else {
        json!({})
    };

    handlebars.set_strict_mode(opt.strict);
    for dir in opt.partial_dirs.iter() {
        handlebars.register_templates_directory(".hbs", dir)?;
    }

    if opt.files.is_empty() {
        eprintln!("No files specified, doing nothing...");
    }

    for file_name in opt.files.iter() {
        let mut file = File::open(file_name)?;

        let mut out: Box<Write> = if let Some(out_dir) = &opt.output {
            let mut file = out_dir.clone();
            file.push(file_name);

            if file.extension() == Some("hbs".as_ref()) {
                file.set_extension("");
            }

            if let Some(parent) = file.parent() {
                fs::create_dir_all(parent)?;
            }

            Box::new(File::create(file)?)
        } else {
            Box::new(stdout())
        };

        handlebars.render_template_source_to_write(&mut file, &data, &mut out)?;
    }

    Ok(())
}
