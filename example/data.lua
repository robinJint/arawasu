local msg = "hello from " .. _VERSION

local list = {}

for i = 1, 3 do
    list[i] = tostring(i) .. "st entry"
end

return {
    msg = msg,
    list = list,
}